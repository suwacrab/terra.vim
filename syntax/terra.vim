" Vim syntax file
" Language:	Terra; for terra, based on terra.vim
" Maintainer:	ura <jenkejam.web.fc2.com>
" URL: https://gitdab.com/suwacrab/terra-vim

if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

let s:cpo_save = &cpo
set cpo&vim

" syncing method
syn sync fromstart

" FIXED fold regions
function! s:FoldableRegion(tag, name, expr)
  let synexpr = 'syntax region ' . a:name . ' ' . a:expr
  let pfx = 'g:terra_syntax_fold_'
  if !exists('g:terra_syntax_nofold') || exists(pfx . a:tag) || exists(pfx . a:name)
    let synexpr .= ' fold'
  end
  exec synexpr
endfunction

" FIXED Clusters
syntax cluster terraBase contains=terraComment,terraCommentLong,terraConstant,terraNumber,terraString,terraStringLong,terraBuiltIn
syntax cluster terraExpr contains=@terraBase,terraTable,terraParen,terraBracket,terraSpecialTable,terraSpecialValue,terraOperator,terraSymbolOperator,terraEllipsis,terraComma,terraFunc,terraFuncCall,terraError
syntax cluster terraStat
      \ contains=@terraExpr,terraIfThen,terraBlock,terraLoop,terraGoto,terraLabel,terraLocal,terraStatement,terraSemiCol,terraErrHand,terraTypeName

" FIXED Symbols
call s:FoldableRegion('table', 'terraTable',
      \ 'transparent matchgroup=terraBraces start="{" end="}" contains=@terraExpr')
syntax region terraParen   transparent matchgroup=terraParens   start='(' end=')' contains=@terraExpr
syntax region terraBracket transparent matchgroup=terraBrackets start="\[" end="\]" contains=@terraExpr
syntax match  terraComma ","
syntax match  terraSemiCol ";"
if !exists('g:terra_syntax_nosymboloperator')
  if exists('g:terra_syntax_fancynotequal')
    syntax match terraNotEqOperator "\V~=" conceal cchar=竕
    setlocal conceallevel=2
  endi
  syntax match terraSymbolOperator "[#<>=~^&|*/%+-]\|\.\." contains=terraNotEqOperator
endi
syntax match  terraEllipsis "\.\.\."

" FIXED Comments
syntax keyword terraCommentTodo contained TODO FIXME XXX TBD
syntax match   terraComment "--.*$" contains=terraCommentTodo,terraDocTag,@Spell
call s:FoldableRegion('comment', 'terraCommentLong',
      \ 'matchgroup=terraCommentLongTag start="--\[\z(=*\)\[" end="\]\z1\]" contains=terraCommentTodo,terraDocTag,@Spell')
syntax match   terraDocTag contained "\s@\k\+"

" Catch errors caused by unbalanced brackets and keywords
syntax match terraError ")"
syntax match terraError "}"
syntax match terraError "\]"
syntax match terraError "\<\%(else\|elseif\|then\|until\)\>"

" Shebang at the start
syntax match terraComment "\%^#!.*"

" FIXED Function calls
syntax match terraFuncCall /\k\+\%(\s*[{('"]\)\@=/

" Functions
call s:FoldableRegion('function', 'terraFunc',
      \ 'transparent matchgroup=terraFuncKeyword start="\<function\>" end="\<end\>" contains=@terraStat,terraFuncSig')
syntax region terraFuncSig contained transparent start="\(\<function\>\)\@<=" end=")" contains=terraFuncId,terraFuncArgs keepend
call s:FoldableRegion('terra', 'terraFunc',
      \ 'transparent matchgroup=terraFuncKeyword start="\<terra\>" end="\<end\>" contains=@terraStat,terraFuncSig')
syntax region terraFuncSig contained transparent start="\(\<terra\>\)\@<=" end=")" contains=terraFuncId,terraFuncArgs keepend
syntax match terraFuncId contained "[^(]*(\@=" contains=terraFuncTable,terraFuncName
syntax match terraFuncTable contained /\k\+\%(\s*[.:]\)\@=/
syntax match terraFuncName contained "[^(.:]*(\@="
syntax region terraFuncArgs contained transparent matchgroup=terraFuncParens start=/(/ end=/)/ contains=@terraBase,terraFuncArgName,terraFuncArgComma,terraEllipsis,terraTypeName
syntax match terraFuncArgName contained /\k\+/
syntax match terraFuncArgComma contained /,/

" if ... then
syntax region terraIfThen transparent matchgroup=terraCond start="\<if\>" end="\<then\>"me=e-4 contains=@terraExpr nextgroup=terraThenEnd skipwhite skipempty

" then ... end
call s:FoldableRegion('control', 'terraThenEnd',
      \ 'contained transparent matchgroup=terraCond start="\<then\>" end="\<end\>" contains=@terraStat,terraElseifThen,terraElse')

" elseif ... then
syntax region terraElseifThen contained transparent matchgroup=terraCond start="\<elseif\>" end="\<then\>" contains=@terraExpr

" else
syntax keyword terraElse contained else

" do ... end
call s:FoldableRegion('control', 'terraLoopBlock',
      \ 'transparent matchgroup=terraRepeat start="\<do\>" end="\<end\>" contains=@terraStat contained')
call s:FoldableRegion('control', 'terraBlock',
      \ 'transparent matchgroup=terraStatement start="\<do\>" end="\<end\>" contains=@terraStat')

" repeat ... until
call s:FoldableRegion('control', 'terraLoop',
      \ 'transparent matchgroup=terraRepeat start="\<repeat\>" end="\<until\>" contains=@terraStat nextgroup=@terraExpr')

" while ... do
syntax region terraLoop transparent matchgroup=terraRepeat start="\<while\>" end="\<do\>"me=e-2 contains=@terraExpr nextgroup=terraLoopBlock skipwhite skipempty

" for ... do and for ... in ... do
syntax region terraLoop transparent matchgroup=terraRepeat start="\<for\>" end="\<do\>"me=e-2 contains=@terraExpr,terraIn nextgroup=terraLoopBlock skipwhite skipempty
syntax keyword terraIn contained in

" other keywords
syn keyword terraStatement return local break
syn keyword terraStatement goto
syn match terraLabel "::\I\i*::"
syn keyword terraOperator and or not
syn keyword terraConstant nil true false

" (more) Terra keywords
syn keyword terraStruct struct union
syn keyword terraVariable var
syn keyword terraTypeName rawstring niltype double float bool int uint int64 uint64 int32 uint32 int16 uint16 int8 uint8

" FIXED Strings
syntax match  terraStringSpecial contained #\\[\\abfnrtvz'"]\|\\x[[:xdigit:]]\{2}\|\\[[:digit:]]\{,3}#
call s:FoldableRegion('string', 'terraStringLong',
      \ 'matchgroup=terraStringLongTag start="\[\z(=*\)\[" end="\]\z1\]" contains=@Spell')
syntax region terraString  start=+'+ end=+'+ skip=+\\\\\|\\'+ contains=terraStringSpecial,@Spell
syntax region terraString  start=+"+ end=+"+ skip=+\\\\\|\\"+ contains=terraStringSpecial,@Spell

" numbers
syn match terraNumber "\<\d\+\>"
" floating point number, with dot, optional exponent
syn match terraNumber  "\<\d\+\.\d*\%([eE][-+]\=\d\+\)\=\>"
" floating point number, starting with a dot, optional exponent
syn match terraNumber  "\.\d\+\%([eE][-+]\=\d\+\)\=\>"
" floating point number, without dot, with exponent
syn match terraNumber  "\<\d\+[eE][-+]\=\d\+\>"

" hex numbers
syn match terraNumber "\<0[xX][[:xdigit:].]\+\%([pP][-+]\=\d\+\)\=\>"

syn keyword terraFunc assert collectgarbage dofile error next
syn keyword terraFunc print rawget rawset tonumber tostring type _VERSION

syn keyword terraFunc getmetatable setmetatable
syn keyword terraFunc ipairs pairs
syn keyword terraFunc pcall xpcall
syn keyword terraFunc _G loadfile rawequal require import
syn keyword terraFunc load select

syn match terraFunc /\<package\.cpath\>/
syn match terraFunc /\<package\.loaded\>/
syn match terraFunc /\<package\.loadlib\>/
syn match terraFunc /\<package\.path\>/
syn keyword terraFunc _ENV rawlen
syn match terraFunc /\<package\.config\>/
syn match terraFunc /\<package\.preload\>/
syn match terraFunc /\<package\.searchers\>/
syn match terraFunc /\<package\.searchpath\>/
syn match terraFunc /\<bit32\.arshift\>/
syn match terraFunc /\<bit32\.band\>/
syn match terraFunc /\<bit32\.bnot\>/
syn match terraFunc /\<bit32\.bor\>/
syn match terraFunc /\<bit32\.btest\>/
syn match terraFunc /\<bit32\.bxor\>/
syn match terraFunc /\<bit32\.extract\>/
syn match terraFunc /\<bit32\.lrotate\>/
syn match terraFunc /\<bit32\.lshift\>/
syn match terraFunc /\<bit32\.replace\>/
syn match terraFunc /\<bit32\.rrotate\>/
syn match terraFunc /\<bit32\.rshift\>/
syn match terraFunc /\<coroutine\.running\>/
syn match terraFunc /\<coroutine\.create\>/
syn match terraFunc /\<coroutine\.resume\>/
syn match terraFunc /\<coroutine\.status\>/
syn match terraFunc /\<coroutine\.wrap\>/
syn match terraFunc /\<coroutine\.yield\>/
syn match terraFunc /\<string\.byte\>/
syn match terraFunc /\<string\.char\>/
syn match terraFunc /\<string\.dump\>/
syn match terraFunc /\<string\.find\>/
syn match terraFunc /\<string\.format\>/
syn match terraFunc /\<string\.gsub\>/
syn match terraFunc /\<string\.len\>/
syn match terraFunc /\<string\.lower\>/
syn match terraFunc /\<string\.rep\>/
syn match terraFunc /\<string\.sub\>/
syn match terraFunc /\<string\.upper\>/
syn match terraFunc /\<string\.gmatch\>/
syn match terraFunc /\<string\.match\>/
syn match terraFunc /\<string\.reverse\>/
syn match terraFunc /\<table\.pack\>/
syn match terraFunc /\<table\.unpack\>/
syn match terraFunc /\<table\.concat\>/
syn match terraFunc /\<table\.sort\>/
syn match terraFunc /\<table\.insert\>/
syn match terraFunc /\<table\.remove\>/
syn match terraFunc /\<math\.abs\>/
syn match terraFunc /\<math\.acos\>/
syn match terraFunc /\<math\.asin\>/
syn match terraFunc /\<math\.atan\>/
syn match terraFunc /\<math\.atan2\>/
syn match terraFunc /\<math\.ceil\>/
syn match terraFunc /\<math\.sin\>/
syn match terraFunc /\<math\.cos\>/
syn match terraFunc /\<math\.tan\>/
syn match terraFunc /\<math\.deg\>/
syn match terraFunc /\<math\.exp\>/
syn match terraFunc /\<math\.floor\>/
syn match terraFunc /\<math\.log\>/
syn match terraFunc /\<math\.max\>/
syn match terraFunc /\<math\.min\>/
syn match terraFunc /\<math\.huge\>/
syn match terraFunc /\<math\.fmod\>/
syn match terraFunc /\<math\.modf\>/
syn match terraFunc /\<math\.cosh\>/
syn match terraFunc /\<math\.sinh\>/
syn match terraFunc /\<math\.tanh\>/
syn match terraFunc /\<math\.pow\>/
syn match terraFunc /\<math\.rad\>/
syn match terraFunc /\<math\.sqrt\>/
syn match terraFunc /\<math\.frexp\>/
syn match terraFunc /\<math\.ldexp\>/
syn match terraFunc /\<math\.random\>/
syn match terraFunc /\<math\.randomseed\>/
syn match terraFunc /\<math\.pi\>/
syn match terraFunc /\<io\.close\>/
syn match terraFunc /\<io\.flush\>/
syn match terraFunc /\<io\.input\>/
syn match terraFunc /\<io\.lines\>/
syn match terraFunc /\<io\.open\>/
syn match terraFunc /\<io\.output\>/
syn match terraFunc /\<io\.popen\>/
syn match terraFunc /\<io\.read\>/
syn match terraFunc /\<io\.stderr\>/
syn match terraFunc /\<io\.stdin\>/
syn match terraFunc /\<io\.stdout\>/
syn match terraFunc /\<io\.tmpfile\>/
syn match terraFunc /\<io\.type\>/
syn match terraFunc /\<io\.write\>/
syn match terraFunc /\<os\.clock\>/
syn match terraFunc /\<os\.date\>/
syn match terraFunc /\<os\.difftime\>/
syn match terraFunc /\<os\.execute\>/
syn match terraFunc /\<os\.exit\>/
syn match terraFunc /\<os\.getenv\>/
syn match terraFunc /\<os\.remove\>/
syn match terraFunc /\<os\.rename\>/
syn match terraFunc /\<os\.setlocale\>/
syn match terraFunc /\<os\.time\>/
syn match terraFunc /\<os\.tmpname\>/
syn match terraFunc /\<debug\.debug\>/
syn match terraFunc /\<debug\.gethook\>/
syn match terraFunc /\<debug\.getinfo\>/
syn match terraFunc /\<debug\.getlocal\>/
syn match terraFunc /\<debug\.getupvalue\>/
syn match terraFunc /\<debug\.setlocal\>/
syn match terraFunc /\<debug\.setupvalue\>/
syn match terraFunc /\<debug\.sethook\>/
syn match terraFunc /\<debug\.traceback\>/
syn match terraFunc /\<debug\.getmetatable\>/
syn match terraFunc /\<debug\.setmetatable\>/
syn match terraFunc /\<debug\.getregistry\>/
syn match terraFunc /\<debug\.getuservalue\>/
syn match terraFunc /\<debug\.setuservalue\>/
syn match terraFunc /\<debug\.upvalueid\>/
syn match terraFunc /\<debug\.upvaluejoin\>/

" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_terra_syntax_inits")
  if version < 508
    let did_terra_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  HiLink terraParens            Noise
  HiLink terraBraces            Structure
  HiLink terraBrackets          Noise
  HiLink terraStatement		Statement
  HiLink terraRepeat		Repeat
  HiLink terraFor		Repeat
  HiLink terraString		String
  HiLink terraString2		String
  HiLink terraNumber		Number
  HiLink terraOperator		Operator
  HiLink terraConstant		Constant
  HiLink terraCond		Conditional
  HiLink terraTypeName          Type
  HiLink terraFunction          Structure
  HiLink terraFuncArgName       Noise
  HiLink terraFuncCall          PreProc
  HiLink terraFuncId            Function
  HiLink terraFuncName          terraFuncId
  HiLink terraFuncTable         terraFuncId
  HiLink terraFuncKeyword       terraFunction
  HiLink terraFunction          Structure
  HiLink terraFuncParens        Noise
  HiLink terraQuote		Function
  HiLink terraComment		Comment
  HiLink terraCommentLongTag    terraCommentLong
  HiLink terraCommentLong       terraComment
  HiLink terraTodo		Todo
  HiLink terraTable		Structure
  HiLink terraStruct		Structure
  HiLink terraError		Error
  HiLink terraParenError	Error
  HiLink terraSpecial		SpecialChar
  HiLink terraFunc		Identifier
  HiLink terraVariable		Identifier
  HiLink terraType		Type
  HiLink terraLabel		Label

  delcommand HiLink
endif

let b:current_syntax = "terra"

let &cpo = s:cpo_save
unlet s:cpo_save
" vim: et ts=8 sw=2